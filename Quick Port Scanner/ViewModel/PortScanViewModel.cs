﻿using Quick_Port_Scanner.Model;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Net.NetworkInformation;
using System.Reflection;
using System.Text;
using System.Threading;
using System.Windows;

namespace Quick_Port_Scanner.ViewModel
{
    class PortScanViewModel
    {
        private Port_Scan _portScanModel = new Port_Scan();

        private string AppTitle;
        private string AppCompany;

        public PortScanViewModel()
        {
            Assembly mainAssembly = Assembly.GetEntryAssembly();
            AssemblyCompanyAttribute companyAttribute = (AssemblyCompanyAttribute)GetAttribute(mainAssembly, typeof(AssemblyCompanyAttribute));
            AssemblyTitleAttribute titleAttribute = (AssemblyTitleAttribute)GetAttribute(mainAssembly, typeof(AssemblyTitleAttribute));
            AppTitle = titleAttribute != null ? titleAttribute.Title : mainAssembly.GetName().Name;
            AppCompany = companyAttribute != null ? companyAttribute.Company : "";
        }

        public void Is_Port_Open(string hostname, string portStr, string timeoutStr)
        {
            ushort port;
            ushort.TryParse(portStr, out port);

            ushort timeout;
            if (!ushort.TryParse(timeoutStr, out timeout))
                timeout = 250;

            bool IsOpen = _portScanModel.Is_Port_Open(hostname, port, timeout / 1000D);

            if (port != 0)
                MessageBox.Show(string.Format("Port {0} is {1}.", port, IsOpen ? "open" : "closed"), AppTitle);
            else
                MessageBox.Show("Invalid port!", AppTitle);
        }

        private static Attribute GetAttribute(Assembly assembly, Type attributeType)
        {
            object[] attributes = assembly.GetCustomAttributes(attributeType, false);
            if (attributes.Length == 0)
            {
                return null;
            }
            return (Attribute)attributes[0];
        }

        public void ScanLocalNetwork()
        {
            var results = _portScanModel.GetNetworkIPs();

            StringBuilder sb = new StringBuilder();

            foreach (var item in results)
                sb.AppendLine(string.Format("{0} - {1}", item.Key, item.Value));

            MessageBox.Show(sb.ToString() + Environment.NewLine + "Total computers found: " + results.Count, AppTitle);
        }
    }
}
