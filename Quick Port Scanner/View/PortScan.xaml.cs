﻿using System.Collections.Generic;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;

namespace Quick_Port_Scanner.View
{
    /// <summary>
    /// Interaction logic for PortScan.xaml
    /// </summary>
    public partial class PortScan : UserControl
    {
        ViewModel.PortScanViewModel viewModel = new ViewModel.PortScanViewModel();

        public PortScan()
        {
            InitializeComponent();

            Loaded += (sender, e) => MoveFocus(new TraversalRequest(FocusNavigationDirection.Next));
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            viewModel.Is_Port_Open(Hostname.Text, Port.Text, Timeout.Text);
        }

        private void TextChanged(object sender, TextChangedEventArgs e)
        {
            SetButtonStatus();
        }

        private void Hostname_KeyDown(object sender, System.Windows.Input.KeyEventArgs e)
        {
            if (e.Key.ToString() == "Return" && CheckButton.IsEnabled)
                Button_Click(null, null);
        }

        private void Port_KeyDown(object sender, System.Windows.Input.KeyEventArgs e)
        {
            TextFilter(e);

            if (e.Key.ToString() == "Return" && CheckButton.IsEnabled)
                Button_Click(null, null);
        }

        private void Timeout_KeyDown(object sender, System.Windows.Input.KeyEventArgs e)
        {
            TextFilter(e);

            if (e.Key.ToString() == "Return" && CheckButton.IsEnabled)
                Button_Click(null, null);
        }

        private void SetButtonStatus()
        {
            HashSet<string> set = new HashSet<string>() { Hostname.Text, Port.Text, Timeout.Text };

            if (set.Contains(string.Empty))
                CheckButton.IsEnabled = false;
            else
                CheckButton.IsEnabled = true;
        }

        private void TextFilter(System.Windows.Input.KeyEventArgs e)
        {
            if (e.Key.ToString() == "D" || e.Key.ToString() == "N" || e.Key.ToString() == "P")
                e.Handled = true;

            if (!"D1D2D3D4D5D6D7D8D9D0NumPad1NumPad2NumPad3NumPad4NumPad5NumPad6NumPad7NumPad8NumPad9NumPad0Tab".Contains(e.Key.ToString()))
                e.Handled = true;
        }

        private void Button_Click_1(object sender, RoutedEventArgs e)
        {
            viewModel.ScanLocalNetwork();
        }
    }
}
