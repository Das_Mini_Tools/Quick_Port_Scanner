﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Net;
using System.Net.NetworkInformation;
using System.Net.Sockets;
using System.Threading;

namespace Quick_Port_Scanner.Model
{
    class Port_Scan
    {
        public bool Is_Port_Open(string hostname, ushort port, double timeout)
        {
            using (TcpClient tcpClient = new TcpClient())
            {
                IAsyncResult result = tcpClient.BeginConnect(hostname, port, null, null);

                result.AsyncWaitHandle.WaitOne(TimeSpan.FromSeconds(timeout));

                if (tcpClient.Connected)
                {
                    tcpClient.EndConnect(result);
                    return true;
                }
                else
                    return false;
            }
        }

        public Dictionary<string,string> GetNetworkIPs()
        {
            Dictionary<string,string> networkComputers = new Dictionary<string,string>(50);

            NetworkBrowser nb = new NetworkBrowser();

            foreach (string pc in nb.GetNetworkComputers())
                networkComputers.Add(pc, Array.FindAll(Dns.GetHostEntry(pc).AddressList, x => x.AddressFamily == AddressFamily.InterNetwork)[0].ToString());

            return networkComputers;
        }
    }
}
